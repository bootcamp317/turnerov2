from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .models import Tickets
from .forms import TicketsModelForm

def Login(request):
    if request.user.is_authenticated:
        return handleLogin(request.user)
    data = request.POST or None
    if data is not None:
        user_name = data.get("user_name")
        user_password  = data.get("user_password")
        user = authenticate(username=user_name, password=user_password)
        if user is not None:
            login(request, user)
            return handleLogin(user)
    return render(request, 'login.html', {})

def handleLogin(user):
    if user.is_superuser:
        return redirect('admin/')
    else:
        return redirect(Main)
    
def Logout(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect(Login) 

#fetching details and saving in a dictionary
# from django.core import serializers

def Main(request):
    # data = serializers.serialize("python",models.Tickets.objects.all())
    # data = serializers.PrimaryKeyRelatedField(queryset=models.Tickets.objects.all())
    # print(data)
    ticket = Tickets.objects.all()
    context = {
        'ticket':ticket,
    }
    if not request.user.is_authenticated:
        return redirect(Login)
    return render(request, 'main.html', context)

def Create(request):
    form = TicketsModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
    if not request.user.is_authenticated:
        return redirect(Login)
    context = {
        "form":form
    }
    return render(request, 'create.html', context)
