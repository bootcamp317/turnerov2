from django.contrib import admin
# from . import models
import django.apps

class CustomAdminArea(admin.AdminSite):
    #Quitar cambiado para el final
    site_header = 'Turnero'
    index_title = 'Administrator'


custom_admin_site = CustomAdminArea(name='CustomAdmin')

#Registrar todos los models
models = django.apps.apps.get_models()
# Imprimir para ver todos los models
# print(models)

for model in models:
    try:
        custom_admin_site.register(model)
    except custom_admin_site.AlreadyRegistered:
        pass

# custom_admin_site.unregister(django.contrib.sessions.models.Session)

# Registrar individualmente
# custom_admin_site.register(models.PriorityLevels)
# custom_admin_site.register(models.ClientConditions)

# Modificar
# class PostAdmin(admin.ModelAdmin):
#     fields = ['title', 'author']

