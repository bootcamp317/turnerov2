from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig

class AdminCustomConfig(AdminConfig):
    default_site = 'application.admin.CustomAdminArea'

class AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'application'
