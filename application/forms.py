from django import forms

from .models import Tickets

class TicketsModelForm(forms.ModelForm):
    class Meta:
        model = Tickets
        fields = ["client_document_number","queue","priority_level","service","status"]
    # def clean_codigo(self):
    #     tickets = self.cleaned_data.get("codigo")
    #     if not codigo == "valido":
    #         raise forms.ValidationError("Ingrese el código (valido)") 
    #     return codigo