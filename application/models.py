# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class PriorityLevels(models.Model):
    id = models.BigAutoField(primary_key=True)
    priority_level_name = models.TextField()

    def __str__(self) -> str:
        return self.priority_level_name

    class Meta:
        verbose_name = "Priority Level"
        verbose_name_plural = "PriorityLevels"
        db_table = 'priority_levels'

class ClientConditions(models.Model):
    id = models.BigAutoField(primary_key=True)
    condition_name = models.TextField()
    priority_level = models.ForeignKey('PriorityLevels', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.condition_name
    
    class Meta:
        verbose_name = "Client Condition"
        verbose_name_plural = "ClientConditions"
        db_table = 'client_conditions'


class Queues(models.Model):
    id = models.BigAutoField(primary_key=True)
    queue_name = models.TextField()

    def __str__(self) -> str:
        return self.queue_name

    class Meta:
        verbose_name = "Queue"
        verbose_name_plural = "Queues"
        db_table = 'queues'


class Services(models.Model):
    id = models.BigAutoField(primary_key=True)
    service_name = models.TextField()

    def __str__(self) -> str:
        return self.service_name

    class Meta:
        verbose_name = "Service"
        verbose_name_plural = "Services"
        db_table = 'services'


class TicketStatuses(models.Model):
    id = models.BigAutoField(primary_key=True)
    status_name = models.TextField()

    def __str__(self) -> str:
        return self.status_name

    class Meta:
        verbose_name = "Ticket Status"
        verbose_name_plural = "TicketStatuses"
        db_table = 'ticket_statuses'


class Tickets(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_timestamp = models.DateTimeField(auto_now=True)
    client_document_number = models.IntegerField()
    queue = models.ForeignKey(Queues, on_delete=models.CASCADE, blank=True, null=True)
    service = models.ForeignKey(Services, on_delete=models.CASCADE, blank=True, null=True)
    priority_level = models.ForeignKey(PriorityLevels, on_delete=models.CASCADE, blank=True, null=True)
    status = models.ForeignKey(TicketStatuses, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"
        db_table = 'tickets'
